package sk.bararaba.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import sk.bararaba.pojos.Bar;
import sk.bararaba.pojos.Foo;

@Mapper
public interface FooBarMapper {

  FooBarMapper INSTANCE = Mappers.getMapper(FooBarMapper.class);

  Foo toFoo(Bar bar);

  Bar toBar(Foo bar);
}
