package sk.bararaba.pojos;

import java.math.BigDecimal;

import org.immutables.value.Value;

@Value.Immutable
public interface Foo {

  @Value.Parameter
  BigDecimal getValue();

}
