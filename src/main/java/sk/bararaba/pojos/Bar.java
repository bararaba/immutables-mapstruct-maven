package sk.bararaba.pojos;

import org.immutables.value.Value;

@Value.Immutable
public interface Bar {

  @Value.Parameter
  String getValue();

}
