package sk.bararaba.mapper;

import java.math.BigDecimal;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import sk.bararaba.pojos.Bar;
import sk.bararaba.pojos.ImmutableFoo;

public class FooBarMapperTest {

  @Test
  public void shouldMapFooToBar() {
    BigDecimal one = BigDecimal.ONE;
    Bar bar = FooBarMapper.INSTANCE.toBar(ImmutableFoo.of(one));

    Assertions.assertThat(bar.getValue()).isEqualTo(one.toString());
  }

}